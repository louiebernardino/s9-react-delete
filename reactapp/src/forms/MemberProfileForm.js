import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const MemberProfileForm = (props, {memberAttr}) => {

  const { firstName, lastName, username, email, teamId, position} = props.member

  return (
    <Form className="m-3">
      <FormGroup>
        <Label for="firstName">First Name</Label>
        <Input type="text" name="firstName" id="firstName" value={firstName}/>
      </FormGroup>
      <FormGroup>
        <Label for="lastName">Last Name</Label>
        <Input type="text" name="lastName" id="lastName" value={lastName}/>
      </FormGroup>
      <FormGroup>
        <Label for="username">Username</Label>
        <Input type="text" name="username" id="username" value={username}>
        </Input>
      </FormGroup>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" name="email" id="email" value={email}>
        </Input>
      </FormGroup>
      <FormGroup>
        <Label for="team">Team</Label>
        <Input type="text" name="team" id="team" value={teamId ? teamId.name: "N/A"}>
        </Input>
      </FormGroup>
      <Button color="primary" size="sm" block>Save Changes</Button>
    </Form>
  );
}

export default MemberProfileForm;