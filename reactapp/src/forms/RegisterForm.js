import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

const RegisterForm = (props) => {
//STATE
//object that holds a component's dynamic data

// console.log(useState("initial value"))
//useState
// const [username, setUsername] = useState("test");
// const [email, setEmail] = useState("test");
// const [password, setPassword] = useState("test");
// const [password2, setPassword2] = useState("test");
// console.log(username);
// console.log(email);
// console.log(password);
// console.log(password2);

const [formData, setFormData] = useState({
  username: "",
  email: "",
  password: "",
  password2: ""
})

const [disabled, setDisabled] = useState(true)

const [isRedirected, setIsRedirected] = useState(false)

const { username, email, password, password2} = formData;
const onChangeHandler = e => {
  // console.log(e);
  // console.log(e.target)

  //Backticks - If we want multi-line and string interpolation
  // console.log(`e.target.name: ${e.target.name}`)
  // console.log(`e.target.value: ${e.target.value}`)

  //update the state
  setFormData({
    //spread operator - it copies whatever value
    ...formData,
    [e.target.name] : e.target.value
  })
  // console.log(formData)
}

const onSubmitHandler = async e => {
  //after clicking submit it will not reload
  e.preventDefault()
  if(password != password2) {
    console.log("Passwords don't match!")
    Swal.fire({
      title: "Error!",
      text: "Passwords don't match!",
      icon: "error",
      showConfirmationButton: false,
      timer: 3000
    })
  } else {
    //submit to backend
    console.log(formData);

    //create a member object
    const newMember = { 
      username,
      email,
      password
    }
    try {
    //create config
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    //create the body
    const body = JSON.stringify(newMember) //JSON newMember converting into a string because you cannot pass string in url
    //create route using axios
    const res = await axios.post("http://localhost:4051/members", body, config) //pass body and config
    console.log(res)

    //Redirect to login
    setIsRedirected(true)
    } catch(e) {
      console.log(e.response.data.message)
    }
  }
}

//USE EFFECT
//React HOOK
//hook that tells the component what to do after render
useEffect(()=>{  
if(username !== "" && email !== "" && password !== "" && password2 !== ""){
  setDisabled(false)
} else {
  setDisabled(true)
}
}, [formData])

if(isRedirected) {
  return <Redirect to="/login"/>
}

  return (
    <Form className="m-3" onSubmit={ e=> onSubmitHandler(e) }>
      <FormGroup>
        <Label for="username">Username</Label>
        <Input type="username" name="username" 
        id="username" value={username} onChange= {e => onChangeHandler(e)}
        maxLength="30" pattern="[a-zA-Z0-9]+" required/>
        <FormText>Use alphanumeric characters only!</FormText>
      </FormGroup>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" name="email" 
        id="email" value={email} onChange= {e => onChangeHandler(e)}
        required/>
      </FormGroup>
      <FormGroup>
        <Label for="password">Password</Label>
        <Input type="password" name="password" 
        id="password" value={password} onChange= {e => onChangeHandler(e)}
        minLength="5"
        required/>
      </FormGroup>
      <FormGroup>
        <Label for="password2">Confirm Password</Label>
        <Input type="password" name="password2" 
        id="password2" value={password2} onChange= {e => onChangeHandler(e)}
        required
        minLength="5"/>
      </FormGroup>
      <Button color="primary" size="sm" block className="mb-3" disabled={disabled}>Submit</Button>
      <p>
        Already have an account? <Link to="/login">click here</Link>
      </p>
    </Form>
  );
}

export default RegisterForm;