import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const TaskProfileForm = (props, {taskAttr}) => {

  const { description, memberId} = props.task

  return (
    <Form className="m-3">
      <FormGroup>
        <Label for="username">Username</Label>
        <Input type="text" name="username" id="username" value={memberId ? memberId.username: "N/A"}/>
      </FormGroup>
      <FormGroup>
        <Label for="memberid">Member Id</Label>
        <Input type="text" name="memberid" id="memberid" value={memberId ? memberId._id : "N/A"}/>
      </FormGroup>
      <FormGroup>
        <Label for="description">Description</Label>
        <Input type="text" name="description" id="description" value={description}>
        </Input>
      </FormGroup>
      <Button color="primary" size="sm" block>Save Changes</Button>
    </Form>
  );
}

export default TaskProfileForm;