import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import Swal from 'sweetalert2';
import axios from 'axios';

const TeamForm = (props) => {

const [formData, setFormData] = useState({
  team: ""
})

const [disabled, setDisabled] = useState(true)

const { team } = formData;
const onChangeHandler = e => {
  // console.log(e);
  // console.log(e.target)

  //Backticks - If we want multi-line and string interpolation
  // console.log(`e.target.name: ${e.target.name}`)
  // console.log(`e.target.value: ${e.target.value}`)

  //update the state
  setFormData({
    [e.target.name] : e.target.value
  })
  // console.log(formData)
}

const onSubmitHandler = async e => {
  //after clicking submit it will not reload
  e.preventDefault()
  if(team == "C" && team == "D") {
    console.log("team doesn't exist!")
    Swal.fire({
      title: "Error!",
      text: "There's no team C and team D on the list!",
      icon: "error",
      showConfirmationButton: false,
      timer: 3000
    })
  } else {
    //submit to backend
    console.log(formData);

    //create a member object
    const newTeam = { 
      team
    }
    try {
    //create config
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    //create the body
    const body = JSON.stringify(newTeam) //JSON newMember converting into a string because you cannot pass string in url
    //create route using axios
    const res = await axios.post("http://localhost:4051/teams", body, config) //pass body and config
    console.log(res)

    } catch(e) {
      console.log(e.response.data.message)
    }
  }
}

//USE EFFECT
//React HOOK
//hook that tells the component what to do after render
useEffect(()=>{  
if(team !== ""){
  setDisabled(false)
} else {
  setDisabled(true)
} 
}, [formData])


  return (
    <Form className="m-3" onSubmit={ e=> onSubmitHandler(e)}>
      <FormGroup>
        <Label for="team">Team</Label>
        <Input type="text" name="team" id="team" value={team} onChange= {e => onChangeHandler(e)}>
        </Input>
      </FormGroup>
      <Button color="primary" size="sm" block disabled={disabled}>Submit</Button>
    </Form>
  );
}

export default TeamForm;