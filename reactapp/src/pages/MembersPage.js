//Dependencies
import React, {useState, useEffect, Fragment} from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import MemberForm from '../forms/MemberForm';
import MemberTable from '../tables/MemberTable';
import MemberModal from '../modals/MemberModal';
import axios from 'axios';


//COMPONENTS
const MembersPage = (props) => { //props - arguments
/*anonymous function*/
//JSX
console.log("memberpage props", props.tokenAttr)

const [ membersData, setMembersData] = useState({
  token: props.tokenAttr,
  members: []
})

const { token, members} = membersData
console.log("MembersPage Token", token)
console.log("MembersPage members", members)

//access members from nodeapp
const getMembers = async (query = "") => {
  try {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }

    const res = await axios.get(`http://localhost:4051/members${query}`, config)

    console.log("Members Res", res)
    return setMembersData({
      ...membersData,
      members: res.data
    })

  } catch(e) {
    console.log(e.response)
    //swal
  }
}

//deleting a member by an admin
const deleteMember = async (id) => {
  try {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }

    const res = await axios.delete(`http://localhost:4051/members/${id}`, config)
    console.log("Delete testing", res.data)
    getMembers()
  } catch(e) {
    //SWAL
    console.log(e)
  }
}

useEffect(()=> {
  getMembers()
}, [setMembersData])

//Update Member: Modal
//declare state
const [modalData, setModalData] = useState({
  modal: false,
  member: {}
}) 

const { modal, member } = modalData

const toggle = async (id) => {
  if (id == 'string') {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }

    const res = await axios.get(`http://localhost:4051/members/${id}`, config)

    return setModalData({
      modal: !modal,
      member: res.data
    })
  }

  return setModalData({
    ...modalData,
    modal: !modal
  })
}

  return (
  <Fragment>
    <Container>
      <Row className="mb-4">
        <Col>
          <h1>Members Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md="4"><MemberForm/></Col>
        <Col>
        <div>
          <Button className="btn-sm border mr-1" onClick={()=> getMembers()}>Get All</Button>
          <Button className="btn-sm border mr-1" onClick={()=> getMembers("?isActive=true")}>Get Active</Button>
          <Button className="btn-sm border mr-1" onClick={()=> getMembers("?isActive=false")}>Get Deactivated</Button>
        </div>
        <hr/>
        <MemberTable membersAttr={members} deleteMember={deleteMember}/></Col>
      </Row>
    </Container>
    <MemberModal toggle={toggle} modal={modal} member={member}/>
  </Fragment>
  );
}

export default MembersPage;

//Discussion
//1.) What is a component?
// - function or class.
// - contains props (arguments)