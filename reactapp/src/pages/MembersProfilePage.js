import React, {useState, useEffect} from 'react'
import { Container, Row, Col } from 'reactstrap' 
import MemberProfileForm from '../forms/MemberProfileForm'
import axios from 'axios';


const MembersProfilePage = (props) => {

const [ memberData, setMemberData] = useState({
	token: props.tokenAttr,
	member:{}
})
const { token, member } = memberData

const getMember = async () => {
	try {
		const config = {
			headers: {
				Authorization: `Bearer ${token}`
			}
		}

		const res = await axios.get(`http://localhost:4051/members/${props.match.params.id}`, config)

		setMemberData({
			...memberData,
			member: res.data
		})
	} catch(e) {
		console.log(e)
	}
}

useEffect(()=>{
	getMember()
}, [setMemberData])

	return (
		<Container className = "my-5">
			<Row className = "mb-3">
				<Col>
					<h1>Member Profile Page</h1>
				</Col>
			</Row>
			<Row className="mb-3">
				<Col>
					<MemberProfileForm member={ member 
					}/>
				</Col>
			</Row>
		</Container>
	)
}

export default MembersProfilePage;