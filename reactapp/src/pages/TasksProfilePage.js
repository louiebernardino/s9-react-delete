import React, {useState, useEffect} from 'react'
import { Container, Row, Col } from 'reactstrap' 
import TaskProfileForm from '../forms/TaskProfileForm'
import axios from 'axios';


const TasksProfilePage = (props) => {

const [ taskData, setTaskData] = useState({
	token: props.tokenAttr,
	task:{}
})
const { token, task } = taskData

const getTask = async () => {
	try {
		const config = {
			headers: {
				Authorization: `Bearer ${token}`
			}
		}

		const res = await axios.get(`http://localhost:4051/tasks/${props.match.params.id}`, config)

		setTaskData({
			...taskData,
			task: res.data
		})
	} catch(e) {
		console.log(e)
	}
}

useEffect(()=>{
	getTask()
}, [setTaskData])

	return (
		<Container className = "my-5">
			<Row className = "mb-3">
				<Col>
					<h1>Task Profile Page</h1>
				</Col>
			</Row>
			<Row className="mb-3">
				<Col>
					<TaskProfileForm task={ task }/>
				</Col>
			</Row>
		</Container>
	)
}

export default TasksProfilePage;