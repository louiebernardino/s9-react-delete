//Dependencies
import React, {useState, useEffect} from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import MemberForm from '../forms/MemberForm';
import MemberTable from '../tables/MemberTable';
import TeamForm from '../forms/TeamForm';
import TeamRow from '../rows/TeamRow';
import TeamTable from '../tables/TeamTable';
import axios from 'axios';


//COMPONENTS
const TeamsPage = (props) => { //props - arguments
/*anonymous function*/
//JSX
const [ teamsData, setTeamsData] = useState({
  token: props.tokenAttr,
  teams: []
})

const { token, teams} = teamsData
console.log("Teams Component", teams)

const getTeams = async (query = "") => {
  try {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }

    const res = await axios.get(`http://localhost:4051/members${query}`, config)

    console.log("Teams Res", res)
    return setTeamsData({
      ...teamsData, 
      teams: res.data
    })

  } catch(e) {
    console.log(e.response)
    //swal
  }
}

const deleteTeam = async (id) => {
  try {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }

    const res = await axios.delete(`http://localhost:4051/teams/${id}`, config)
    console.log("Delete testing", res.data)
    getTeams()
  } catch(e) {
    //SWAL
    console.log(e)
  }
}

//getTeams()
useEffect(()=> {
  getTeams()
}, [setTeamsData])

console.log("Team", props.getCurrentMemberAttr)
  return (
    <Container>
      <Row className="mb-4">
        <Col>
          <h1>Teams Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md="4" className="border"><TeamForm/></Col>
        <Col>
        <div>
          <Button className="btn-sm border mr-1" onClick={()=> getTeams()}>Get All</Button>
          <Button className="btn-sm border mr-1" onClick={()=> getTeams("?isActive=true")}>Get Active</Button>
          <Button className="btn-sm border mr-1" onClick={()=> getTeams("?isActive=false")}>Get Deactivated</Button>
        </div>
        <hr/>
        <TeamTable teamsAttr={teams} deleteTeam={deleteTeam}/></Col>
      </Row>
    </Container>
  );
}

export default TeamsPage;

//Discussion
//1.) What is a component?
// - function or class.
// - contains props (arguments)